using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

public class Coin : MonoBehaviour
{
    [field: SerializeField] public int Score { get; private set; }
    [SerializeField] private TextMeshPro nameTxt;

    private void OnEnable()
    {
        Score = Random.Range(1, 5);
        nameTxt.SetText($"Coinx{Score}");
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.TryGetComponent<Player>(out var _player))
        {
            OnPlayerCollected(_player);
        }
    }

    private void OnPlayerCollected(Player _player)
    {
        AddScore(Score);
        gameObject.SetActive(false);
        if (GameplayManager.Instance.TryGetManager<RespawnManager>(out var _respawnManager))
        {
            _respawnManager.StartRespawn(gameObject);
        }
    }

   private void AddScore(int _score)
    {
        ScoreManager.Instance.AddScore(_score);
    }
}
