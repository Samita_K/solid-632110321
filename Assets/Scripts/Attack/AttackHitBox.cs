using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackHitBox : MonoBehaviour
{
    [SerializeField] private AttackController _attackController;
    [SerializeField] private GameObject owner;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.GetComponent<IDamageable>() != null && col.gameObject != owner )
        {
            col.GetComponent<IDamageable>().ApplyDamage(gameObject, _attackController.GetWeapon().Damage);
        }
    }
}
