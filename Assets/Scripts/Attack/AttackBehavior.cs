using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AttackBehavior
{
    protected AttackController host;
    public AttackBehavior(AttackController _host){}
    
    public abstract void PerformAttack();
}
