using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

public class Player : MonoBehaviour, IDamageable
{
    public static event Action<WeaponData> OnChangedWeapon;
    public UnityEvent<int> onHpChanged;

    private const int MaxHp = 5;

    [SerializeField] private Rigidbody2D rb;

    [Header("Health")] 
    [SerializeField] private int currentHp = MaxHp;
    
    /*[Header("Attack")]
    [SerializeField] private WeaponData currentWeapon;
    [SerializeField] private SpriteRenderer weaponSprite;
    [SerializeField] private Transform firePoint;
    [SerializeField] private GameObject meleeHitBox;
    private bool isAttacking; */
    
    [SerializeField] private Transform spawnPoint;
    private Vector2 moveInput;
    private bool isGrounded;
    

    private void Start()
    {
        onHpChanged?.Invoke(currentHp);
    }
    
    #region HP

    public void Heal(int _value)
    {
        currentHp += _value;
        onHpChanged?.Invoke(currentHp);
    }
    
    public void DecreaseHp(int _value)
    {
        currentHp -= _value;
        
        if (currentHp <= 0)
        {
            Death();
        }
        onHpChanged?.Invoke(currentHp);
    }

    private void Death()
    {
        currentHp = MaxHp;
        Respawn();
    }

    private void Respawn()
    {
        rb.velocity = Vector2.zero;
        transform.position = spawnPoint.position;
    }
    
    #endregion

    public void ApplyDamage(GameObject _source, int _damage)
    {
        DecreaseHp(_damage);
    }
}
